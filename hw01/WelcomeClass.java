// Molly Benning
// 1/29/18
// CSE2 Welcome Class
public class WelcomeClass {
  
  public static void main (String[] args) {
    // Prints text signature on the terminal
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-M--I--B--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v ");
    System.out.println("Hi! My name is Molly and I'm from the DC area! I'm studying ISE and I'm very involved in theater here on campus!");
  }
  
}