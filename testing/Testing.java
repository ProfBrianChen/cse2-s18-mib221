import java.util.Scanner;
public class Testing {
  
  public static void main (String[] args){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the value of an integer: ");
    int val1 = myScanner.nextInt();
    System.out.print("Enter the value of another integer: ");
    int val2 = myScanner.nextInt();
    
    if(val1 > val2){
      System.out.println(val1 + ", " + val2);
    }
    else if(val2 > val1){
      System.out.println(val2 + ", " + val1);
    }

  }
}