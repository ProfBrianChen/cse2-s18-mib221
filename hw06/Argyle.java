// Molly Benning
// 3/10/18
// CSE2 Argyle
//
import java.util.Scanner;

public class Argyle {
  
  public static void main (String[] args){
    // declare scanners
    Scanner widthScan = new Scanner( System.in );
    Scanner heightScan = new Scanner( System.in );
    Scanner argyleSizeScan = new Scanner( System.in );
    Scanner stripeSizeScan = new Scanner( System.in );
    Scanner symbol1Scan = new Scanner( System.in );
    Scanner symbol2Scan = new Scanner( System.in );
    Scanner symbol3Scan = new Scanner( System.in );
    // declare int values
    int width;
    int height;
    int argyleSize;
    int stripeSize;
    int stripeSizeHalf;
    // get user input for width
    System.out.print("Please enter a positive integer for the width of Viewing window in characters: ");
    // ensure value is positive and an integer
    while (true){
      if (widthScan.hasNextInt() == false){
        System.out.print("Entered value must be an integer: ");
        widthScan.next();
      }
      else {
        width = widthScan.nextInt();
        if (width > 0){
          break;
        }
        else {
          System.out.print("Entered value must be positive: ");
        }
      }
    }
        // get user input for height
    System.out.print("Please enter a positive integer for the height of Viewing window in characters: ");
    // ensure value is positive and an integer
    while (true){
      if (heightScan.hasNextInt() == false){
        System.out.print("Entered value must be an integer: ");
        heightScan.next();
      }
      else {
        height = heightScan.nextInt();
        if (height > 0){
          break;
        }
        else {
          System.out.print("Entered value must be positive: ");
        }
      }
    }
        // get user input for width of diamond
    System.out.print("Please enter a positive integer for the width of the argyle diamond: ");
    // ensure value is positive and an integer
    while (true){
      if (argyleSizeScan.hasNextInt() == false){
        System.out.print("Entered value must be an integer: ");
        argyleSizeScan.next();
      }
      else {
        argyleSize = argyleSizeScan.nextInt();
        if (argyleSize > 0){
          break;
        }
        else {
          System.out.print("Entered value must be positive: ");
        }
      }
    }
        // get user input for size of the stripe
    System.out.print("Please enter a positive odd integer for the width of the argyle center stripe: ");
    // ensure value is positive, odd, and an integer
    while (true){
      if (stripeSizeScan.hasNextInt() == false){
        System.out.print("Entered value must be an integer: ");
        stripeSizeScan.next();
      }
      else {
        stripeSize = stripeSizeScan.nextInt();
        if (stripeSize > 0){
          if (stripeSize % 2 == 1){
            if (argyleSize / 2 >= stripeSize){
              break;
            }
            else {
              System.out.print("Entered value cannot be larger than half the size of the diamond: ");
            }
          }
          else {
            System.out.print("Entered value must be odd: ");
          }
        }
        else {
          System.out.print("Entered value must be positive: ");
        }
      }
    }
    stripeSize -= 1;
    stripeSizeHalf = stripeSize / 2;
    // get user input for symbol 1
    System.out.print("Please enter a first character for the pattern fill: ");
    String temp1 = symbol1Scan.next();
    char symbol1 = temp1.charAt(0);
    // get user input for symbol 2
    System.out.print("Please enter a second character for the pattern fill: ");
    String temp2 = symbol2Scan.next();
    char symbol2 = temp2.charAt(0);
    // get user input for symbol 3
    System.out.print("Please enter a third character for the stripe fill: ");
    String temp3 = symbol3Scan.next();
    char symbol3 = temp3.charAt(0);
    // print the argyle
    for (int i = 0; i < argyleSize; i++){
      for (int j = 0; j < argyleSize; j++){
        if (i == j || (j <= (i + stripeSizeHalf) && j >= (i - stripeSizeHalf))){
          System.out.print(symbol3);
        }
        else if ((i > (argyleSize - (j + 1)) && i <= argyleSize) || (i < (argyleSize - (j + 1)) && i >= argyleSize)){
          System.out.print(symbol1);
        }
        else {
          System.out.print(symbol2);
        }
      }
      for (int j = 0; j < argyleSize; j++){
        if (((argyleSize - (j + 1)) == i) || (j >= ((argyleSize - stripeSizeHalf) - (i + stripeSizeHalf)) && j < ((argyleSize - stripeSizeHalf) - (i - stripeSize)))){
          System.out.print(symbol3);
        }
        else if (((i > j) && i <= argyleSize) || ((i < j) && i >= argyleSize)){
          System.out.print(symbol1);
        }
        else {
          System.out.print(symbol2);
      }
     }
      System.out.println();
    }
    for (int i = 0; i < argyleSize; i++){
      for (int j = 0; j < argyleSize; j++){
        if (((argyleSize - (j + 1)) == i) || (j >= ((argyleSize - stripeSizeHalf) - (i + stripeSizeHalf)) && j < ((argyleSize - stripeSizeHalf) - (i - stripeSize)))){
          System.out.print(symbol3);
        }
        else if (((i > j) && i <= argyleSize) || ((i < j) && i >= argyleSize)){
          System.out.print(symbol2);
        }
        else {
          System.out.print(symbol1);
        }
      }
      for (int j = 0; j < argyleSize; j++){
        if (i == j || (j <= (i + stripeSizeHalf) && j >= (i - stripeSizeHalf))){
          System.out.print(symbol3);
        }
        else if ((i > (argyleSize - (j + 1)) && i <= argyleSize) || (i < (argyleSize - (j + 1)) && i >= argyleSize)){
          System.out.print(symbol2);
        }
        else {
          System.out.print(symbol1);
    }
   }
      System.out.println();
  }   
 }
}