// Molly Benning
// 2/2/18
// CSE2 Cyclometer
// Prints # minutes, # counts, distance,
// and the distance of two trips on a bicycle
//
public class Cyclometer {
  
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      // create variables
      	int secsTrip1=480;  // seconds to complete trip 1
       	int secsTrip2=3220;  // seconds to complete trip 2
		    int countsTrip1=1561;  // rotations of the wheel to complete trip 1
		    int countsTrip2=9037; // rotations of the wheel to complete trip 2
        double wheelDiameter=27.0,  // diameter of wheel
      	PI=3.14159, // value for pi
  	    feetPerMile=5280,  // feet in a mile
  	    inchesPerFoot=12,   // inches in a foot
  	    secondsPerMinute=60;  // seconds in a minute
	      double distanceTrip1, distanceTrip2,totalDistance;  // distances of the seperate trips and combined trips
      // print time taken to complete trips
         System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      // runs the calculations and stores its values
	  	// calculates distance by multiplying the rotation of the wheel
	  	// by the diameter of the wheel by pi
	        distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	        distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	        distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	        totalDistance=distanceTrip1+distanceTrip2;
      // print distances
       System.out.println("Trip 1 was "+distanceTrip1+" miles");
	     System.out.println("Trip 2 was "+distanceTrip2+" miles");
	     System.out.println("The total distance was "+totalDistance+" miles");


      
	}  //end of main method   
} //end of class
