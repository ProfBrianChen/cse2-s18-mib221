// Molly Benning
// 03/29/19
// CSE02 Lab07

import java.util.Random;
import java.util.Scanner;

public class Lab07 {
  
  public static void main(String[] args){
    do {
    Random randomGenerator = new Random();
      Scanner answerScan = new Scanner( System.in );
    int f = randomGenerator.nextInt(10);
    int g = randomGenerator.nextInt(10);
    int h = randomGenerator.nextInt(10);
    int i = randomGenerator.nextInt(10);
    int j = randomGenerator.nextInt(10);
    String a = adjective(f);
    String b = subject(g);
    String c = verbs(h);
    String d = adjective(i);
    String e = object(j);
    System.out.println("The " + a + " " + b + " " + c + " the " + d + " " + e + ".");
      System.out.print("Would you like another sentence? (Type Y for yes): ");
      String answer = answerScan.next();
      if (answer.equals("Y")){
      }
      else {
        break;
      }
    } while (true);
  }
  public static String adjective(int random){
    String adj = "test";
    switch (random) {
      case 0: adj = "pointless";
        break;
      case 1: adj = "longing";
        break;
      case 2: adj = "beneficial";
        break;
      case 3: adj = "stupid";
        break;
      case 4: adj = "gifted";
        break;
      case 5: adj = "squeamish";
        break;
      case 6: adj = "tranquil";
        break;
      case 7: adj = "ragged";
        break;
      case 8: adj = "volatile";
        break;
      case 9: adj = "apathetic";
    }
    return adj;
  }
  public static String subject(int random){
    String sub = "test";
    switch (random) {
      case 0: sub = "boy";
        break;
      case 1: sub = "girl";
        break;
      case 2: sub = "turtle";
        break;
      case 3: sub = "elephant";
        break;
      case 4: sub = "teacher";
        break;
      case 5: sub = "kitten";
        break;
      case 6: sub = "duck";
        break;
      case 7: sub = "lion";
        break;
      case 8: sub = "hamster";
        break;
      case 9: sub = "axolotl";
    }
    return sub;
  }
  public static String verbs(int random){
    String verb = "test";
      switch (random) {
      case 0: verb = "scolded";
        break;
      case 1: verb = "rinsed";
        break;
      case 2: verb = "melted";
        break;
      case 3: verb = "pinched";
        break;
      case 4: verb = "examined";
        break;
      case 5: verb = "remembered";
        break;
      case 6: verb = "damaged";
        break;
      case 7: verb = "chopped";
        break;
      case 8: verb = "knitted";
        break;
      case 9: verb = "missed";
    }
    return verb;
  }
  public static String object(int random){
    String obj = "test";
      switch (random) {
      case 0: obj = "sweater";
        break;
      case 1: obj = "cake";
        break;
      case 2: obj = "mozzarella stick";
        break;
      case 3: obj = "friend";
        break;
      case 4: obj = "exam";
        break;
      case 5: obj = "necklace";
        break;
      case 6: obj = "pencil";
        break;
      case 7: obj = "cactus";
        break;
      case 8: obj = "bicycle";
        break;
      case 9: obj = "sneaker";
    }
    return obj;
  }
}