// Molly Benning
// 2/16/18
// CSE2 Card Generator
//
public class CardGenerator {
  
  public static void main (String[] args){
    // declare card values 1-52
    int card1 = (int)(Math.random()*52)+1;
    int card2 = (int)(Math.random()*52)+1;
    int card3 = (int)(Math.random()*52)+1;
    int card4 = (int)(Math.random()*52)+1;
    // declare suits
    String cardValue1;
    String cardValue2;
    String cardValue3;
    String cardValue4;
    // sort cards into suits
    if( card1 <= 13 ){ // card 1 diamonds
      cardValue1 = "diamonds";
    }
        else if( card1 <= 26 ){ // card 1 clubs
          cardValue1 = "clubs";
        }
          else if( card1 <= 39 ){ // card 1 hearts
            cardValue1 = "hearts";
          }
            else{ // card 1 spades
              cardValue1 = "spades";
            }
     if( card2 <= 13 ){ // card 2 diamonds
      cardValue2 = "diamonds";
    }
        else if( card2 <= 26 ){ // card 2 clubs
          cardValue2 = "clubs";
        }
          else if( card2 <= 39 ){ // card 2 hearts
            cardValue2 = "hearts";
          }
            else{ // card 2 spades
              cardValue2 = "spades";
            }
     if( card3 <= 13 ){ // card 3 diamonds
      cardValue3 = "diamonds";
    }
        else if( card3 <= 26 ){ // card 3 clubs
          cardValue3 = "clubs";
        }
          else if( card3 <= 39 ){ // card 3 hearts
            cardValue3 = "hearts";
          }
            else{ // card 3 spades
              cardValue3 = "spades";
            }
     if( card4 <= 13 ){ // card 4 diamonds
      cardValue4 = "diamonds";
    }
        else if( card4 <= 26 ){ // card 4 clubs
          cardValue4 = "clubs";
        }
          else if( card4 <= 39 ){ // card 4 hearts
            cardValue4 = "hearts";
          }
            else{ // card 4 spades
              cardValue4 = "spades";
            }
    // set card value to be between 1-13
    if( card1 <= 52 && card1 > 39 ){ // card 1
      card1 = card1 - 39;
    }
    else if( card1 > 26 ){
      card1 = card1 - 26;
    }
    else if( card1 > 13){
      card1 = card1 - 13;
    }
    if( card2 <= 52 && card2 > 39 ){ // card 2
      card2 = card2 - 39;
    }
    else if( card2 > 26 ){
      card2 = card2 - 26;
    }
    else if( card2 > 13){
      card2 = card2 - 13;
    }
    if( card3 <= 52 && card3 > 39 ){ // card 3
      card3 = card3 - 39;
    }
    else if( card3 > 26 ){
      card3 = card3 - 26;
    }
    else if( card3 > 13){
      card3 = card3 - 13;
    }
    if( card4 <= 52 && card4 > 39 ){ // card 4
      card4 = card4 - 39;
    }
    else if( card4 > 26 ){
      card4 = card4 - 26;
    }
    else if( card4 > 13){
      card4 = card4 - 13;
    }
    // replace 1,11,12,13 with face cards
    switch (card1) {
      case 1: System.out.println("Card 1 is the ace of " + cardValue1);
        break;
      case 11: System.out.println("Card 1 is the jack of " + cardValue1);
        break;
      case 12: System.out.println("Card 1 is the queen of " + cardValue1);
        break;
      case 13: System.out.println("Card 1 is the king of " + cardValue1);
        break;
      default: System.out.println("Card 1 is the " + card1 + " of " + cardValue1);
    }
    switch (card2) {
      case 1: System.out.println("Card 2 is the ace of " + cardValue2);
        break;
      case 11: System.out.println("Card 2 is the jack of " + cardValue2);
        break;
      case 12: System.out.println("Card 2 is the queen of " + cardValue2);
        break;
      case 13: System.out.println("Card 2 is the king of " + cardValue2);
        break;
      default: System.out.println("Card 2 is the " + card2 + " of " + cardValue2);
  }
    switch (card3) {
      case 1: System.out.println("Card 3 is the ace of " + cardValue3);
        break;
      case 11: System.out.println("Card 3 is the jack of " + cardValue3);
        break;
      case 12: System.out.println("Card 3 is the queen of " + cardValue3);
        break;
      case 13: System.out.println("Card 3 is the king of " + cardValue3);
        break;
      default: System.out.println("Card 3 is the " + card3 + " of " + cardValue3);
}
    switch (card4) {
      case 1: System.out.println("Card 4 is the ace of " + cardValue4);
        break;
      case 11: System.out.println("Card 4 is the jack of " + cardValue4);
        break;
      case 12: System.out.println("Card 4 is the queen of " + cardValue4);
        break;
      case 13: System.out.println("Card 4 is the king of " + cardValue4);
        break;
      default: System.out.println("Card 4 is the " + card4 + " of " + cardValue4);
    }
  }
}