// Molly Benning
// 4/20/18
// CSE2 Lab10

import java.util.Random; // creates a random int

public class Lab10 {
  
  public static int[][] increasingMatrix(int width, int height, boolean format){ // creates the matrix
    if (format == true && width != 0 && height != 0){ // ensures the parameters are non-zero integers (format true)
      int[][] matrix = new int[width][height];
      int k = 1; // initiates value of matrix
      for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
          matrix[j][i] = k; // sets matrix value to k
          k += 1; // increases k by one
        }
      }
      return matrix;
    }
    else if (format == false && width != 0 && height != 0) { // ensures the parameters are non-zero integers (format false)
      int[][] matrix = new int[height][width];
      int k = 1; // initiates value of matrix
      for (int i = 0; i < height; i++){
        for (int j = 0; j < width; j++){
          matrix[i][j] = k; // sets matrix value to k
          k += height; //increases k along the row by the height
        }
        k += 1 - (height * width); // increases k by one and subtracts the height by the width
      }
      return matrix;
    }
    else { // makes the parameters zero
      return null;
    }
  }
  public static void printMatrix(int[][] matrix, boolean format){ // prints out the matrix
    if (matrix == null){ // checks if the array has anything in it
      System.out.println("The array was empty!");
    }
    else if (format == true){ // prints in row format
      for (int i = 0; i < matrix[0].length; i++){
        for (int j = 0; j < matrix.length; j++){
          System.out.printf("%3d ", matrix[j][i]); // %3d creates a set number of spaces between values
        }
        System.out.println();
      }
    }
    else if (format == false){ // prints in column format
      for (int i = 0; i < matrix.length; i++){
        for (int j = 0; j < matrix[0].length; j++){
          System.out.printf("%3d ", matrix[i][j]);
        }
        System.out.println();
      }
    }
  }
  public static int[][] translate(int[][] matrix){ // translates column forms to row forms
    if (matrix == null){ // checks if there's anything in the array
      System.out.println("The array was empty!");
      return null;
    }
    else {
    int[][] array = new int[matrix[0].length][matrix.length];
    int k = 1;
    for (int i = 0; i < matrix.length; i++){ // resets the matrix to be in row form
      for (int j = 0; j < matrix[0].length; j++){
        array[j][i] = k;
        k += 1;
      }
    }
    return array;
    }
  }
  public static int[][] addMatrix(int[][] a, boolean formatA, int[][] b, boolean formatB){ // adds two matrices together
    if (a == null || b == null){ // checks if any of the matrices are empty
      System.out.println("The array was empty!");
      return null;
    }
    else {
      if (formatA == false){ // translates column form matrix to row form
        System.out.println("Translating Matrix to row-major form:");
        a = translate(a);
      }
      if(formatB == false){ // translates column form matrix to row form
        System.out.println("Translating Matrix to row-major form:");
        b = translate(b);
      }
    int[][] sum = new int[a.length][a[0].length]; // initiates matrix to have the correct dimensions
    if ((a.length == b.length) && (a[0].length == b[0].length)){ // checks if the dimensions of both matrices are the same
      for (int i = 0; i < a[0].length; i++){
        for (int j = 0; j < a.length; j++){
          sum[j][i] = a[j][i] + b[j][i]; // adds the matrices
          System.out.printf("%3d ", sum[j][i]);
        }
        System.out.println();
      }
      return sum;
    }
    else { // if the matrices have different dimensions
      System.out.println("The arrays cannot be added!");
      return null;
    }
    }
  }
  public static void main(String[] args){
    Random rand = new Random();
    int height1 = rand.nextInt(10);
    int width1 = rand.nextInt(10);
    int height2 = rand.nextInt(10);
    int width2 = rand.nextInt(10);
    boolean formatA = true;
    boolean formatB = false;
    int[][] matrixA = increasingMatrix(width1, height1, formatA);
    int[][] matrixB = increasingMatrix(width1, height1, formatB);
    int[][] matrixC = increasingMatrix(width2, height2, formatA);
    System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
    printMatrix(matrixA, formatA);
    System.out.println();
    System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
    printMatrix(matrixB, formatB);
    System.out.println();
    System.out.println("Generating a matrix with width " + height2 + " and height " + width2 + ":");
    printMatrix(matrixC, formatB);
    System.out.println();
    System.out.println("Adding Matrix A to Matrix B:");
    addMatrix(matrixA, formatA, matrixB, formatB);
    System.out.println();
    System.out.println("Adding Matrix A to Matrix C:");
    addMatrix(matrixA, formatA, matrixC, formatB);
  }
}