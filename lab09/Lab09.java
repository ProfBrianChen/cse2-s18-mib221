// Molly Benning
// 4/13/18
// CSE2 Lab09

public class Lab09 {
  
  public static int[] copy(int[] array){ // method that copies arrays
    int[] arrayCopy = new int[8];
    for (int i = 0; i < 8; i++){
      arrayCopy[i] = array[i];
    }
    return arrayCopy;
  }
  public static void inverter(int[] array){ // method that inverts and rewrites an array
    for (int i = 0; i < 8; i++){
      array[i] = 8 - i;
    }
  }
  public static int[] inverter2(int[] array){ // method that inverts an array
    int[] arrayCopy = copy(array);
    for (int i = 0; i < 8; i++){
      arrayCopy[i] = 8 - i;
    }
    return arrayCopy;
  }
  public static void print(int[] array){ // method that prints an array
    System.out.print("{ ");
    for (int i = 0; i < 8; i++){
      System.out.print(array[i] + " ");
    }
    System.out.println("}");
  }
  public static void main(String[] args){
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0); // inverts array0
    print(array0); // prints the inverted array
    inverter2(array1); // inverts the copy of array1
    print(array1); // prints array1
    int[] array3 = inverter2(array2); // sets array3 equal to the inverted copy of array2
    print(array3); // prints array3
  }
}