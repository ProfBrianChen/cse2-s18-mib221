// Molly Benning
// 3/6/18
// CSE2 Twist Generator
//
import java.util.Scanner;
//
public class TwistGenerator {
  
  public static void main (String[] args){
    Scanner length = new Scanner( System.in ); // input for length of twist
    // get user input for length of twist
    System.out.print("Enter the desired length of the twist: ");
    // determine if number is an int
    while (true){
      if (length.hasNextInt() == true){
        int intLength = length.nextInt(); // save input as an int
        // print out twist
        int counter = 0;
        while (counter < intLength){
          System.out.print("\\");
          ++counter;
          if (counter == intLength){
            break;
          }
          System.out.print(" ");
          ++counter;
          if (counter == intLength){
            break;
          }
          System.out.print("/");
          ++counter;
        }
        System.out.print("\n");
        counter = 0;
        while (counter < intLength){
          System.out.print(" ");
          ++counter;
          if (counter == intLength){
            break;
          }
          System.out.print("x");
          ++counter;
          if (counter == intLength){
            break;
          }
          System.out.print(" ");
          ++counter;
        }
        System.out.print("\n");
        counter = 0;
        while (counter < intLength){
          System.out.print("/");
          ++counter;
          if (counter == intLength){
            break;
          }
          System.out.print(" ");
          ++counter;
          if (counter == intLength){
            break;
          }
          System.out.print("\\");
          ++counter;
        }
        System.out.print("\n");
        break;
      }
      else {
        System.out.print("Length must be an integer: ");
        length.next();
      }
    }
  }
}