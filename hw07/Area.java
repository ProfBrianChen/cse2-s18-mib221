// Molly Benning
// 3/27/18
// CSE02 Area

import java.util.Scanner;

public class Area {
  
  public static void main(String[] args){
    Scanner inputScan = new Scanner( System.in );
    Scanner input2 = new Scanner( System.in );
    // determine the shape being calculated
    System.out.print("Enter the name of a shape (either rectangle, triangle, or circle): ");
    String input = inputScan.next();
    while (true) {
    if (input.equals("rectangle")){ // determines if the input is a rectangle
      System.out.print("Enter the height of the rectangle: ");
      while (true){ // ensures the value is a double
        if (input2.hasNextDouble() == false){
          System.out.print("Value must be a double: ");
          input2.next();
        }
        else {
          double height = input2.nextDouble();
          System.out.print("Enter the width of the rectangle: ");
          while (true){ // ensures the value is a double
            if (input2.hasNextDouble() == false){
              System.out.print("Value must be a double: ");
              input2.next();
            }
            else {
              double width = input2.nextDouble();
              double value = rectangle(height, width); // pulls from rectangle method
              System.out.println("The area of the rectangle is " + value);
              break;
        }
       }
          break;
      }
     }
      break;
    }
    else if (input.equals("triangle")){ // determines if input is a triangle
      System.out.print("Enter the height of the triangle: ");
      while (true){ // ensures the value is a double
        if (input2.hasNextDouble() == false){
          System.out.print("Value must be a double: ");
          input2.next();
        }
        else {
          double height = input2.nextDouble();
          System.out.print("Enter the width of the triangle: ");
          while (true){ // ensures the value is a double
            if (input2.hasNextDouble() == false){
              System.out.print("Value must be a double: ");
              input2.next();
            }
            else {
              double width = input2.nextDouble();
              double value = triangle(height, width); // pulls from triangle method
              System.out.println("The area of the triangle is " + value);
              break;
        }
       }
          break;
      }
     }
      break;
    }
    else if (input.equals("circle")){ // determines if the input is a circle
      System.out.print("Enter the radius of the circle: ");
      while (true){ // ensures values is a double
        if (input2.hasNextDouble() == false) {
          System.out.print("Value must be a double: ");
          input2.next();
        }
        else {
          double radius = input2.nextDouble();
          double value = circle(radius); // pulls from circle method
          System.out.println("The area of the circle is " + value);
          break;    
      }
     }   
      break;
    }
    else { // tells user to enter one of the valid shapes
      System.out.print("The shape needs to be either a rectangle, triangle, or circle: ");
      input = inputScan.next();
    }
   }
  }
  // rectangle method
  public static double rectangle(double a, double b){
    double area = a * b; // width * height
    return area;
  } 
  // triangle method
  public static double triangle(double a, double b){
    double area = (a * b) / 2; // width * height * (1/2)
    return area;
  }
  // circle method
  public static double circle(double a){
    double area = (a * a) * 3.14159265; // (pi)r^2
    return area;
  }
}