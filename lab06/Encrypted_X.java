// Molly Benning
// 3/9/18
// CSE2 Encrypted X
//
import java.util.Scanner;

public class Encrypted_X {
  
  public static void main (String[] args){
    // declare a scanner
    Scanner input = new Scanner( System.in );
    // get user input
    System.out.print("Enter an integer between 0 and 100: ");
    int intInput;
    // determine if value is an int
    while (true){
      if (input.hasNextInt() == false){
        System.out.print("You need to enter an integer: ");
        input.next();
      }
      else {
        // determine if value is within range
        intInput = input.nextInt();
        if (intInput >= 0 && intInput <= 100){
          break;
        }
        else {
          System.out.print("Variable needs to be between 0 and 100: ");
        }
      }
    }
    // create encrypted x
    for (int i = 0; i < intInput; i++){ // row
      for (int j = 0; j < intInput; j++){ // column
        // determine if the space printed needs to be space or a star
        if (i == j || (intInput - (j + 1)) == i){
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
      }
      System.out.println();
    }
  }
}