// Molly Benning
// 3/5/18
// CSE2 Hw05
//
import java.util.Scanner;
//
public class Hw05 {
  
  public static void main (String[] args){
    // declare Scanners
    Scanner courseNum = new Scanner( System.in );
    Scanner deptName = new Scanner( System.in );
    Scanner numOfMeets = new Scanner( System.in );
    Scanner time = new Scanner( System.in );
    Scanner instName = new Scanner( System.in );
    Scanner studNum = new Scanner( System.in );
    // get user input for course number
    System.out.print("Enter the course number: ");
    // determine if number is an int
    while (true){
      if (courseNum.hasNextInt() == true){
        break;
      }
      else {
        System.out.print("You need to enter an integer: ");
        courseNum.next();
      }
    }
    // get user input for department name
    System.out.print("Enter the department name: ");
    // determine if input is a string
    while (true){
      if (deptName.hasNextInt() == false && deptName.hasNextDouble() == false && deptName.hasNextBoolean() == false && deptName.hasNextFloat() == false){
        break;
      }
      else {
        System.out.print("You need to enter a string: ");
        deptName.next();
      }
    }
    // get user input for number of meetings
    System.out.print("Enter the number of meetings in a week: ");
    // determine if input is an integer
    while (true){
      if (numOfMeets.hasNextInt() == true){
        break;
      }
      else {
        System.out.print("You need to enter an integer: ");
        numOfMeets.next();
      }
    }
    // get user input for time of meetings
    System.out.print("Enter the time of the meetings in military time: ");
    // determine if input is a valid integer
    while (true){
      if (time.hasNextInt() == true){
        break;
      }
      else {
        System.out.print("You need to enter an integer: ");
        time.next();
      }
    }
    // get user input for instructor name
    System.out.print("Enter the name of the instructor: ");
    // determine if the input is a string
    while (true){
      if (instName.hasNextInt() == false && instName.hasNextDouble() == false && instName.hasNextFloat() == false && instName.hasNextBoolean() == false){
        break;
      }
      else {
        System.out.print("You need to enter a string: ");
        instName.next();
      }
    }
    // get user input for the number of students
    System.out.print("Enter the number of students in class: ");
    // determine if the input is an integer
    while (true){
      if (studNum.hasNextInt() == true){
        break;
      }
      else {
        System.out.print("You need to enter an integer: ");
        studNum.next();
      }
    }
  }
}