// Molly Benning
// 4/24/18
// CSE2 Robot City

import java.util.Random;

public class RobotCity {
  
  public static int[][] buildCity(int we, int ns){ // builds the city
    Random rand = new Random();
    int[][] city = new int[we][ns]; // makes city limits random ints
    for (int i = 0; i < ns; i++){
      for (int j = 0; j < we; j++){
        city[j][i] = rand.nextInt(899) + 100; // builds population for each block
      }
    }
    return city;
  }
  public static void display(int[][] city, int we, int ns){ // displays the population of each block
    for (int i = 0; i < ns; i++){
      for (int j = 0; j < we; j++){
        System.out.printf("%4d ", city[j][i]);
      }
      System.out.println();
    }
  }
  public static int[][] invade(int k, int[][] city, int we, int ns){ // randomly assigns a robot to a block
    Random rand = new Random();
    for (int i = k; i > 0;){
      int rand1 = rand.nextInt(we); // random row coordinate
      int rand2 = rand.nextInt(ns); // random column coordinate
      if (city[rand1][rand2] > 0){
        city[rand1][rand2] = -1 * city[rand1][rand2]; // makes block negative if it's not already negative
        i -= 1;
      }
    }
    for (int i = 0; i < ns; i++){
      for (int j = 0; j < we; j++){
        System.out.printf("%4d ", city[j][i]); // displays the blocks with the new robot overlords
      }
      System.out.println();
    }
    return city; // makes the rest of my life easier
  }
  public static void update(int[][] city, int we, int ns){ // moves the robots to the east
    for (int i = 0; i < ns; i++){
      for (int j = 1; j < we; j++){
        if (city[we - j][i] < 0){ // sees if far east column is negative
          city[we - j][i] = -1 * city[we - j][i]; // makes it positive
        }
        if (city[we - j - 1][i] < 0 && city[we - j][i] > 0){ // checks if the column before is negative and if the column currently is positive
          city[we - j][i] = -1 * city[we - j][i]; // changes current column to negative
          city[we - j - 1][i] = -1 * city[we - j - 1][i]; // changes previous column to positive
        }
      }
    }
    for (int i = 0; i < ns; i++){
      for (int j = 0; j < we; j++){
        System.out.printf("%4d ", city[j][i]); // displays updated blocks
      }
      System.out.println();
    }
  }
  public static void main(String[] args){
    Random rand = new Random();
    int we = rand.nextInt(5) + 10;
    int ns = rand.nextInt(5) + 10;
    int k = rand.nextInt(ns * we);
    int[][] city = buildCity(we, ns);
    System.out.println("Ah yes, a lovely city!");
    display(city, we, ns);
    System.out.println();
    System.out.println("INVASION!!!!");
    invade(k, city, we, ns);
    System.out.println();
    System.out.println("THEY'VE BEGUN ADVANCING EASTWARDS!!!!");
    for (int i = 0; i < 5; i++){
      update(city, we, ns);
      System.out.println();
    }
    System.out.println("We have accepted our new robot overlords."); // these were just to make it not just a sad large quantity of arrays
  }
}