// Molly Benning
// 2/2/18
// CSE2 Arithmetic
//
public class Arithmetic {
  
  public static void main (String[] args){
    // declare assumptions
    int numPants = 3; // number of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
    int numShirts = 2; // number of sweatshirts
    double shirtPrice = 24.99; // cost per shirt
    int numBelts = 1; // number of belts
    double beltCost = 33.99; // cost per belt
    double paSalesTax = 0.06; // the tax rate
    // declare total price variables
    double totalCostOfPants; // total cost of pants
    totalCostOfPants = numPants * pantsPrice; // number of pants multiplied by pants cost
    double totalCostOfShirts; // total cost of shirts
    totalCostOfShirts = numShirts * shirtPrice; // number of shirts multiplied by shirt cost
    double totalCostOfBelts; // total cost of belts
    totalCostOfBelts = numBelts * beltCost; // number of belts multiplied by belt cost;
    // declare sales tax variables
    double pantsTax; // cost of pants tax
    pantsTax = totalCostOfPants * paSalesTax; // total cost of pants multiplied by the sales tax
    double shirtsTax; // cost of shirt tax
    shirtsTax = totalCostOfShirts * paSalesTax; // total cost of shirts multiplied by the sales tax
    double beltTax; // cost of belt tax
    beltTax = totalCostOfBelts * paSalesTax; // total cost of belts multiplied by the sales tax
    // decrease the decimal places of the sales tax
    pantsTax *= 100;
    pantsTax = (int) pantsTax;
    pantsTax /= 100;
    shirtsTax *= 100;
    shirtsTax = (int) shirtsTax;
    shirtsTax /= 100;
    beltTax *= 100;
    beltTax = (int) beltTax;
    beltTax /= 100;
    // find total cost of purchase without sales tax
    double totalCostOfItems; // cost of items combined without sales tax
    totalCostOfItems = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // cost of each item added together
    // find the total sales tax
    double totalSalesTax; // cost of sales tax combined
    totalSalesTax = pantsTax + shirtsTax + beltTax; // sales taxes of items added together
    totalSalesTax *= 100;
    totalSalesTax = (int) totalSalesTax;
    totalSalesTax /= 100;
    // find the full price paid for objects
    double totalPrice; // full price paid
    totalPrice = totalCostOfItems + totalSalesTax; // cost of items added to sales tax
    totalPrice *= 100;
    totalPrice = (int) totalPrice;
    totalPrice = totalPrice / 100;
    // print values
    System.out.println("Cost of the pants bought is $" + totalCostOfShirts + " and the sales tax is $" + pantsTax);
    System.out.println("Cost of the shirts bought is $" + totalCostOfShirts + " and the sales tax is $" + shirtsTax);
    System.out.println("Cost of the belts bought is $" + totalCostOfBelts + " and the sales tax is $" + beltTax);
    System.out.println("The total cost of the items bought without sales tax is $" + totalCostOfItems);
    System.out.println("The total sales tax paid is $" + totalSalesTax);
    System.out.println("The full price paid is $" + totalPrice);
    
  }
}

