// Molly Benning
// 2/12/18
// CSE2 Convert
//
import java.util.Scanner; // allow scanners
//
public class Convert {
  
  public static void main (String[] args){
    // declare scanner
    Scanner myScanner = new Scanner( System.in );
    // get user input for acres of land
    System.out.print("Enter the affected area in acres: ");
    double area = myScanner.nextDouble(); // variable that holds area in acres
    // get user input for rainfall in inches
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = myScanner.nextDouble(); // variable that holds rainfall in inches
    // convert acres to square feet
    area *= 43560; // value of area in square feet
    // convert inches to feet
    rainfall /= 12; // value of rainfall in feet
    // multiply the area by the rainfall
    double average;
    average = area * rainfall; // value of amount of water on land in cubic feet
    // convert cubic feet to cubic miles
    average /= 1.47197952e11; // value of amount of water on land in cubic miles
    // print results
    System.out.println(average + " cubic miles");
    
  }
}
