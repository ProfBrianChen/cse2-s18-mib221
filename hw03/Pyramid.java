// Molly Benning
// 2/12/18
// CSE2 Pyramid
//
import java.util.Scanner; // allows scanners
//
public class Pyramid {
  
  public static void main (String[] args){
    // declare scanner
    Scanner myScanner = new Scanner( System.in );
    // get user input for square side of pyramid
    System.out.print("Enter the value for the length of the square side of the pyramid: ");
    double squareSide = myScanner.nextDouble(); // variable which holds value of the square side length
    // get user input for the height of the pyramid
    System.out.print("Enter the value for the height of the pyramid: ");
    double height = myScanner.nextDouble(); // variable which holds the value of the height
    // declare variables needed
    double squareArea; // area of square base
    squareArea = Math.pow(squareSide, 2);
    // calculate volume
    double volume; // volume of pyramid
    volume = squareArea * (height / 3); // a^2(h/3)
    // print results
    System.out.println("The volume inside the pyramid is: " + volume);
  }
}