// Molly Benning
// 2/9/18
// CSE2 Check
//
import java.util.Scanner; // allow scanners
//
public class Check{
  // main method
  public static void main(String[] args) {
    // declare scanner
    Scanner myScanner = new Scanner( System.in );
    // get user input of cost of check
    System.out.print("Enter the orignial cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble(); // variable that holds check cost
    // get user input for tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble(); // variable that holds the tip percentage
    // convert percentage into a decimal value
    tipPercent /= 100;
    // get user input of number of people at dinner
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); // variable that holds the number of people at dinner
    // set extra variables
    double totalCost; // total cost of the dinner
    double costPerPerson; // cost of the dinner each person must pay
    int dollars, // whole dollar amount of cost
    dimes,
    pennies; // for storing digits to the right of the decimal point for the cost
    // do the calculations for the total cost
    totalCost = checkCost * (1 + tipPercent); // cost of check times the percentage of the tip + 100%
    // calculations for cost per person
    costPerPerson = totalCost / numPeople; // total cost divided by the number of people at dinner
    // get the whole amount, sans decimal fraction
    dollars = (int) costPerPerson;
    // get dimes amount, e.g.
    dimes = (int) (costPerPerson * 10) % 10; // % operator gives remainder after division
    pennies = (int) (costPerPerson * 100) % 10; // same as above
    // print the price paid by each person
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  } // end of main method
} // end of class